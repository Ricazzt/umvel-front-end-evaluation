import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CarGraphComponent } from './pages/carGraph/carGraph.component';

// Rutas del modulo CarGraph
const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: CarGraphComponent},
      { path: '**', redirectTo: '' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CarGraphRoutingModule { }
