import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
import { LoaderService } from 'src/app/core/services/loader.service';
import { CarGraphService } from 'src/app/core/services/carGraph.service';

@Component({
  selector: 'app-car-graph',
  templateUrl: './carGraph.component.html',
  styleUrls: ['./carGraph.component.scss']
})
export class CarGraphComponent implements OnInit {

  public carGraphData: any[] = [];
  // Objetos de ng2-charts para la gráfica
  public lineChartData: ChartDataSets[] = [
    { data: [],
      label: 'Ventas',
      borderWidth: 2,
      radius: 4
    }
  ];
  public lineChartLabels: Label[] = [];
  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      xAxes: [{}],
      yAxes: [{}]
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
        },
      ],
    },
  };
  public lineChartColors: Color[] = [
    {
      backgroundColor: '#0060643d',
      borderColor: '#006064',
      pointBackgroundColor: '#002828',
      pointBorderColor: '#00989e',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend = false;
  public lineChartType: ChartType = 'line';

  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective | undefined;

  constructor(
    private carsGraphService: CarGraphService,
    private loadService: LoaderService
  ) { }

  ngOnInit(): void {
    // Se cargan los datos para la gráfica al inicair la vista
    this.loadCarGraphData();
  }

  // Metodo para cargar los gatos de la gráfica
  loadCarGraphData(): void {
    this.loadService.startLoader();
    this.carsGraphService.getCarGraphData().subscribe(
      (data: any) => {
        this.carGraphData = [... data];
        // Se mapea el array de datos para obtenerlos en el formato requerido por la gráfica
        this.lineChartData[0].data = this.carGraphData.map((x) => x.quantity);
        this.lineChartLabels = this.carGraphData.map((x) => x.car_make);
        this.loadService.stopLoader();
       }, (err) => {}
     );
  }
}
