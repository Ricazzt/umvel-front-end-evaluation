import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarGraphRoutingModule } from './carGraph-routing.module';
import { CarGraphComponent } from './pages/carGraph/carGraph.component';
import { ChartsModule } from 'ng2-charts';


@NgModule({
  declarations: [CarGraphComponent],
  imports: [
    CommonModule,
    CarGraphRoutingModule,
    ChartsModule
  ]
})
export class CarGraphModule { }
