import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../../../core/authentication/auth.service';
import { StorageService } from '../../../../core/authentication/storage.service';
import { Session } from '../../../../shared/models/session';
import { LoginObject } from '../../../../shared/models/loginObject';
import { LoaderService } from 'src/app/core/services/loader.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public loginFormControl: FormGroup = new FormGroup({});
  public hidePassword = true;
  public loginError = false;
  public submitted = false;

  constructor(
    private fBuilder: FormBuilder,
    private authService: AuthService,
    private storageService: StorageService,
    private router: Router,
    private loadService: LoaderService
  ) { }

  ngOnInit(): void {
    // Se degfinen las validaciones para el formulario de login
    this.loginFormControl = this.fBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(4)]]
    });
  }

  // Si el login es correcto se manda a guardar la sesion actual en el localStorage y se avanza al modulo de dashboard
  private correctLogin(data: Session): void {
    this.storageService.setCurrentSession(data);
    this.router.navigate(['/dashboard']);
  }

  // Metodo para mandar los datos del formulario al servidory obtener su respuesta
  public submitLogin(): void {
    this.loadService.startLoader();
    this.loginError = false;
    if (this.loginFormControl.valid && !this.submitted){
      this.submitted = true;
      this.authService.login(new LoginObject(this.loginFormControl.value)).subscribe(
        (data) => {
          data ? this.correctLogin(data) : this.loginError = true;
          this.submitted = false;
          this.loadService.stopLoader();
        }, (err) => {}
      );
    }
  }
}
