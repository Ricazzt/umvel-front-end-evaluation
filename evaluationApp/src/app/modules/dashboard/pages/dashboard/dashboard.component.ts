import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/core/authentication/storage.service';
import { User } from 'src/app/shared/models/user';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public profileData: User = new User();

  constructor(public storageService: StorageService) { }

  ngOnInit(): void {
    // Se obtienen los datos del usuario de la sesión actual
    this.profileData = this.storageService.getCurrentUser();
  }

}
