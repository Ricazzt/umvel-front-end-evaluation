import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersComponent } from './pages/users/users.component';

// Rutas del modulo Users
const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: UsersComponent},
      { path: '**', redirectTo: '' }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
