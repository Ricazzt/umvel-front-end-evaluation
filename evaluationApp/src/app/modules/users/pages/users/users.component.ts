import { AfterViewInit, Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { UsersService } from 'src/app/core/services/users.service';
import { User } from 'src/app/shared/models/user';
import { StorageService } from 'src/app/core/authentication/storage.service';
import { LoaderService } from 'src/app/core/services/loader.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, AfterViewInit {

  // Obejeto de Columnas de la tabla
  public displayedColumns: string[] = ['position', 'image', 'name', 'lastName', 'email', 'gender'];
  // Obejeto de datos para la tabla de Material
  public dataSource = new MatTableDataSource<User>([]);
  public username: string = '';
  // Paginador de material
  @ViewChild(MatPaginator) paginator: MatPaginator | any;

  constructor(
    private userService: UsersService,
    private storageService: StorageService,
    private loadService: LoaderService
  ) { }

  ngOnInit(): void {
    // Se obtiene el nombre del usuario actual
    this.username = this.storageService.getCurrentUsername();
    // Se cargan los datos de los usuarios
    this.loadUserData();
  }

  ngAfterViewInit(): void {
    // Inicialización del paginador
    this.dataSource.paginator = this.paginator;
  }

  // Método para cargar los datos de los usuarios desde user service
  loadUserData(): void {
    this.loadService.startLoader();
    this.userService.getUsersData().subscribe(
      (data: any) => {
        // Se guardan los datos de los usuarios
        this.dataSource = new MatTableDataSource<User>([... data.users]);
        // Se actualiza el paginador de material
        this.dataSource.paginator = this.paginator;
        this.loadService.stopLoader();
       }, (err) => {}
     );
  }
}
