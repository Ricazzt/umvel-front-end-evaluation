import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pageTitle'
})
export class PageTitlePipe implements PipeTransform {

  // Pipe para dar formato al titulo de las paginas principales

  transform(value: any, args?: any): any {
    let txt = '';
    switch (value){
      case '/dashboard': txt = 'DASHBOARD'; break;
      case '/users': txt = 'USUARIOS'; break;
      case '/car-graph': txt = 'GRÁFICA DE CARROS'; break;
      default: txt = value.toUpperCase().substring(1, value.length);
    }
    return txt;
  }

}
