export class LoginObject {
  // Modelo para los datos del fromulario de login
    public email: string;
    public password: string;

    constructor( data: any){
      this.email = data.email ? data.email : null;
      this.password = data.password ? data.password : null;
    }
}
