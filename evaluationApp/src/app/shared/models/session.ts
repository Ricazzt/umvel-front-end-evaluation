import { User } from './user';

export class Session {
    // Modelo para los datos de Sesion
    public token: string;
    public user: User;

    constructor(){
        this.token = '';
        this.user = new User();
    }
}
