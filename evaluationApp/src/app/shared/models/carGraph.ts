export class CarGraph {
    // Modelo para los datos de los carros
    public id: number;
    public date: string;
    public quantity: number;
    public car_make: string;

    constructor(data?: any){
        this.id = (data && data.id) ? data.id : null;
        this.date = (data && data.date) ? data.date : null;
        this.quantity = (data && data.quantity) ? data.quantity : null;
        this.car_make = (data && data.car_make) ? data.car_make : null;
    }
}

