export class User {
    // Modelo para los datos de usuario
    public id: number;
    public first_name: string;
    public last_name: string;
    public email: string;
    public gender: string;
    public image: string;
    public password?: string;

    constructor(data?: any){
        this.id = (data && data.id) ? data.id : null;
        this.first_name = (data && data.first_name) ? data.first_name : null;
        this.last_name = (data && data.last_name) ? data.last_name : null;
        this.email = (data && data.email) ? data.email : null;
        this.gender = (data && data.gender) ? data.gender : null;
        this.image = (data && data.image) ? data.image : null;
    }
}

