import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { StorageService } from '../authentication/storage.service';

@Injectable({
  providedIn: 'root'
})

// Guard para las rutas que requieren que NO se tenga autenticación
export class NotAuthGuard implements CanActivate {

  constructor(
    private router: Router,
    private storageService: StorageService
  ) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

      if (!this.storageService.isAuthenticated()){
        return true;
      }
      return this.router.parseUrl('/dashboard');
  }
}
