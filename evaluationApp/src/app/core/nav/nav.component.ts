import { Component, OnInit } from '@angular/core';
import { Router  } from '@angular/router';
import { StorageService } from '../authentication/storage.service';
import { MatDialog} from '@angular/material/dialog';
import { LogoutDialog } from './logoutDialog/logoutDialog';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  // Rutas del sidevar
  public navItems = [
    { name: 'Dashboard', icon: 'dashboard', route: '/dashboard'},
    { name: 'Users', icon: 'people', route: '/users'},
    { name: 'Car Graph', icon: 'directions_car', route: '/car-graph'}
  ];

  constructor(
    public router: Router,
    public storageService: StorageService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
  }

  // Metodo para abrir el dialogo de Material
  public openLogoutDialog(): void {
    const dialogRef = this.dialog.open(LogoutDialog, {
      width: '300px',
      data: { name: this.storageService.getCurrentUsername() }
    });

    // Callback al cerrar el dialogo
    dialogRef.afterClosed().subscribe(result => {
      if (result){
        // Se manda a cerrar la sesión de usuario
        this.storageService.logout();
      }
    });
  }
}

