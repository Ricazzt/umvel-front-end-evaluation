import {Component, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-logout-dialog',
  templateUrl: './logoutDialog.html',
  styleUrls: ['./logoutDialog.scss']
})

export class LogoutDialog {
  constructor(
      public dialogRef: MatDialogRef<LogoutDialog>,
      @Inject(MAT_DIALOG_DATA) public data: { name: string}
  ) {}

  // Función para cerrar el dialogo
  onNoClick(): void {
    this.dialogRef.close();
  }
}
