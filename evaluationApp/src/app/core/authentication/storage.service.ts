import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Session } from 'src/app/shared/models/session';
import { User } from 'src/app/shared/models/user';

@Injectable()
export class StorageService {

  private localStorageService: any;
  private currentSession: any;

  // Se inicializan las variables localStorageService y currentSession
  constructor(private router: Router) {
    this.localStorageService = localStorage;
    this.currentSession = this.loadStorageSession();
  }

  // Metodo para cargar la sesión actual de localStorage
  loadStorageSession(): any{
    const sessionStr = this.localStorageService.getItem('currentSession');
    return (sessionStr) ? <Session> JSON.parse(sessionStr) : null;
  }

  // Metodo para guardar en localStorage el usuario actual logeado
  setCurrentSession(session: Session): void {
    this.currentSession = session;
    this.localStorageService.setItem('currentSession', JSON.stringify(session));
  }

  // Metodo para obtener Session
  getCurrentSession(): Session {
    return this.currentSession;
  }

  // Metodo para obtener el usuario
  getCurrentUser(): any {
    const session: Session = this.getCurrentSession();
    return (session && session.user) ? session.user : null;
  }

  // Metodo para obtener el nombre del usuario
  getCurrentUsername(): any {
    const user: User = this.getCurrentUser();
    return (user && user.first_name) ? user.first_name : null;
  }

  // Metodo para obtener la imagen del usuario
  getCurrentUserImage(): any {
    const user: User = this.getCurrentUser();
    return (user && user.image) ? user.image : null;
  }

  // Metodo para obtener el token del usuario
  getSessionToken(): any {
    const session = this.getCurrentSession();
    return (session && session.token) ? session.token : null;
  }

  // Metodo para verificar si el usuario esta logeado
  isAuthenticated(): boolean {
    return this.getSessionToken() ? true : false;
  }

  // Metodo para limpiar Session
  cleanSession(): void {
    this.localStorageService.removeItem('currentSession');
    this.currentSession = null;
  }

  // Metodo para el logout
  logout(): void{
    this.cleanSession();
    this.router.navigate(['/home']);
  }
}