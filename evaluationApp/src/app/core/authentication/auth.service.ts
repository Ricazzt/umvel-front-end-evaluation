import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LoginObject } from 'src/app/shared/models/loginObject';
import { Session } from 'src/app/shared/models/session';
import { User } from 'src/app/shared/models/user';
import { UsersService } from '../services/users.service';

@Injectable()
export class AuthService {

  constructor(
    private userService: UsersService
  ) {}
  // Metodo que simula un login
  login(loginObj: LoginObject): Observable<any> {
    return new Observable(obs => {
      const newSession = new Session();
      // No tenemos un servidor, por lo que se hace un get del Json de usuarios y se valida si existe uno con el email ingresado.
      this.userService.getUsersData().subscribe(
       (data: any) => {
         // Como no hay password esta no se valida.
          const newUser = data.users.find((u: any) => u.email === loginObj.email);
          if (newUser){
            newSession.user = new User(newUser);
            // Se le asigna un token 'inventado' solo para tener una sesion completa.
            newSession.token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c';
            // Devolvemos la sesion actual
            return obs.next(newSession);
          } else {
            return obs.next(null);
          }
        }, (err) => {
          console.error(err);
          return obs.next(null);
        }
      );
    });
  }
}
