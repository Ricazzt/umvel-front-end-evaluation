import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { LoaderComponent } from './nav/loader/loader.component';
import { NavComponent } from './nav/nav.component';
import { FooterComponent } from './footer/footer.component';
import { AuthService } from './authentication/auth.service';
import { CarGraphService } from './services/carGraph.service';
import { LoaderService } from './services/loader.service';
import { StorageService } from './authentication/storage.service';
import { UsersService } from './services/users.service';
import { LogoutDialog } from './nav/logoutDialog/logoutDialog';
import { PageTitlePipe } from '../shared/pipes/pageTitle.pipe';

// Componentes de material
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@NgModule({
  declarations: [NavComponent, FooterComponent, LoaderComponent, LogoutDialog, PageTitlePipe],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    RouterModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatTooltipModule,
    MatProgressSpinnerModule
  ],
  exports: [
    NavComponent , FooterComponent, LoaderComponent, LogoutDialog
  ],
  providers: [
    AuthService,
    StorageService,
    UsersService,
    CarGraphService,
    LoaderService
  ]
})
export class CoreModule { }
