import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class LoaderService {

  private loader = new BehaviorSubject<boolean>(false);
  private loader$ = this.loader.asObservable();

  constructor() { }

  // Se devuelve el observable loader$
  public getLoaderStatus(): Observable<boolean>{
    return this.loader$;
  }

  // Se cambia el valor del observable a true
  public startLoader(): void{
    this.loader.next(true);
  }

  // Se cambia el valor del observable a false
  public stopLoader(): void{
    this.loader.next(false);
  }
}
