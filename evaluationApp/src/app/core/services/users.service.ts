import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from 'src/app/shared/models/user';

@Injectable()
export class UsersService {

  // Ruta del Json de usuarios
  private usersPath: string = 'https://run.mocky.io/v3/d5ddf1ff-a0e2-4a7e-bbcc-e832bef6a503';

  constructor(private http: HttpClient) {}

  getUsersData(): Observable<any> {
    return this.http.get(this.usersPath);
  }

  getUsersDataPaginator(pageIndex: any, pageSize: any): Observable<any> {
    return this.http.get(this.usersPath);
  }
}
