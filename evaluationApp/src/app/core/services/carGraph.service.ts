import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CarGraph } from 'src/app/shared/models/carGraph';

@Injectable()
export class CarGraphService {

  // Ruta del Json de carros
  private carsPath: string = 'https://run.mocky.io/v3/15517ca5-7e07-4ebc-bf63-5b033ec4e16a';

  constructor(private http: HttpClient) {}

  // Metodo para obtener los carros con un get desde carsPath
  getCarsData(): Observable<any> {
    return this.http.get(this.carsPath);
  }

  // Metodo para hacer la sumatoria por marca de carro
  getQuantity(array: CarGraph[], name: string): number {
    return array.reduce((sum: number, val: CarGraph) =>
      ((typeof val.quantity === 'number') && (val.car_make === name) ? sum + val.quantity : sum), 0);
  }

  // Metodo pra devolver los datos para la grafica de carros
  getCarGraphData(): Observable<any> {
    return new Observable(obs => {
      // Se define el arreglo que tendrá objetos con quantity y car_make
      const carsData: {quantity: number, car_make: string}[] = [];
      // Se obtienen los datos de carros desde getCarsData
      this.getCarsData().subscribe(
       (data: any) => {
         // Se itera el arreglo de carros
          data.sales.forEach((e: CarGraph) => {
              if (!carsData.find(val => val.car_make === e.car_make)) {
                // Solo si NO existe un objeto con el nombre de la marca actual se genera un objeto nuevo yse hace push en carsData
                carsData.push({
                  // Se obtiene la sumatoria de quantity por marca con getQuantity
                  quantity: this.getQuantity(data.sales, e.car_make),
                  car_make: e.car_make
                });
              }
          });
          // Se ordena alfabeticamente el array de acuerdo a car_make
          carsData.sort((a, b) => {
            return a.car_make.localeCompare(b.car_make);
          });
          return obs.next(carsData);
        }, (err) => {
          console.error(err);
          return obs.next(null);
        }
      );
    });
  }

}
