# Umvel-Front-end-Evaluation

Proyecto para la evaluación front-end de Umvel.

# Versiones:

Node: 14.16.0

Angular CLI: 11.2.5

Angular: 11.2.6

# Instrucciones:

Antes de iniciar el proyecto ejecutar "npm install".

Ejecutar "ng serve" para inicializar el proyecto en un servidor de desarrollo (`http://localhost:4200/`).

# NOTA:

Tomar en cuenta que para iniciar sesión es necesario ingresar un "email valido", se toman como validos los de los usuarios de este Json: (`https://run.mocky.io/v3/d5ddf1ff-a0e2-4a7e-bbcc-e832bef6a503`). La contraseña puede ser cuenquiera mayor a 3 digitos.
